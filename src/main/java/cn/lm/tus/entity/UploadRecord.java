package cn.lm.tus.entity;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Objects;

import cn.hutool.core.util.IdUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author cuijiaxu@zhichubao.com 2021/7/8 15:41
 */
@Setter
@Getter
@NoArgsConstructor
public class UploadRecord {

    /**
     * 文件全局唯一ID
     */
    private String guid = IdUtil.objectId();
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件桶名
     */
    private String bucket;
    /**
     * 文件总长度
     */
    private long uploadLength;
    /**
     * 文件已上传长度，偏移量
     */
    private long offset = 0;
    /**
     * 文件是否上传完成
     */
    private boolean isCompleted = false;
    /**
     * 文件记录到期时间
     */
    private LocalDateTime endTime;

    public UploadRecord(Map<String, String> matedataHeader, long uploadLength, long timeout) {
        Objects.requireNonNull(matedataHeader, "文件元数据为null");
        this.bucket = matedataHeader.get("bucket");
        this.fileName = matedataHeader.get("filename");
        this.uploadLength = uploadLength;
        this.endTime = LocalDateTime.now().plus(timeout, ChronoUnit.MILLIS);
    }

    public String uniqueKey() {
        return guid + "@" + fileName;
    }

    public String mainFile(){
        return this.guid + "_main";
    }

    public String partFile(){
        return guid + "_part";
    }

    public String mainTempFile(){
        return guid + "_main.temp";
    }
}
