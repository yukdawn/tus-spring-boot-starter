package cn.lm.tus.exception;

/**
 * @author cuijiaxu@zhichubao.com 2021/7/9 10:51
 */
public class FileServerException extends RuntimeException{

    public FileServerException(Throwable cause) {
        super(cause);
    }
}
