package cn.lm.tus.consts;

/**
 * @author cuijiaxu@zhichubao.com 2021/7/8 15:31
 */
public class TusConsts {
    public static final String TUS_RESUMABLE_HEADER = "Tus-Resumable";
    public static final String TUS_RESUMABLE_VALUE = "1.0.0";

    public static final String TUS_VERSION_HEADER = "Tus-Version";
    public static final String TUS_VERSION_VALUE = "1.0.0,0.2.2,0.2.1";

    public static final String TUS_EXTENTION_HEADER = "Tus-Extension";
    public static final String TUS_EXTENTION_VALUE = "creation,expiration,checksum";

    public static final String TUS_MAX_SIZE_HEADER = "Tus-Max-Size";

    public static final String TUS_CHECKSUM_ALGORITHM_HEADER = "Tus-Checksum-Algorithm";
    public static final String TUS_CHECKSUM_ALGORITHM_VALUE = "sha1,crc32";

    public static final String UPLOAD_OFFSET_HEADER = "Upload-Offset";
    public static final String UPLOAD_LENGTH_HEADER = "Upload-Length";
    public static final String UPLOAD_METADATA_HEADER = "Upload-Metadata";
    public static final String UPLOAD_EXPIRES_HEADER = "Upload-Expires";
    public static final String UPLOAD_CHECKSUM_HEADER = "Upload-Checksum";

    public static final String LOCATION_HEADER = "Location";

    public static final String ACCESS_CONTROL_EXPOSE_HEADER = "Access-Control-Expose-Headers";
    public static final String ACCESS_CONTROL_EXPOSE_OPTIONS_VALUE = "Tus-Resumable, Tus-Version, Tus-Max-Size, Tus-Extension";
    public static final String ACCESS_CONTROL_EXPOSE_POST_VALUE = "Location, Tus-Resumable";
    public static final String ACCESS_CONTROL_EXPOSE_HEAD_VALUE = "Upload-Offset, Upload-Length, Tus-Resumable";
    public static final String ACCESS_CONTROL_EXPOSE_PATCH_VALUE = "Upload-Offset, Tus-Resumable";

    public static final String CACHE_CONTROL_HEADER = "Cache-Control";
    public static final String CACHE_CONTROL_VALUE = "no-store";

    public static final String TUS_BUCKET = "tus";

    public static final String DEFAULT_BUCKET = "tus_default";

    public static final String RFC_7231 = "EEE, dd MMM yyyy HH:mm:ss z";
}
