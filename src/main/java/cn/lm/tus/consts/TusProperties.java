package cn.lm.tus.consts;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author cuijiaxu@zhichubao.com 2021/7/9 14:22
 */
@Getter
@Setter
@ConfigurationProperties("tus")
@Component
public class TusProperties {

    private Long tusMaxSize = 500L * 1024 * 1024;

    private String tusPrefix = "/tus";

    private Long tusUploadTimeout = 24L * 60 * 60 * 1000;

    private String minioHost;

    private String minioUsername;

    private String minioPassword;

    public String tusPattern() {
        return tusPrefix + "/*";
    }

    public String tusGuidAntPattern() {
        return tusPrefix + "/{guid}";
    }
}
