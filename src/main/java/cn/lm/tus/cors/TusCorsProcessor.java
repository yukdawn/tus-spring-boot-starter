package cn.lm.tus.cors;

import java.io.IOException;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.StrUtil;
import cn.lm.tus.consts.TusProperties;
import cn.lm.tus.consts.TusConsts;
import cn.lm.tus.entity.UploadRecord;
import cn.lm.tus.service.TusMetadataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.DefaultCorsProcessor;

/**
 * @author cuijiaxu@zhichubao.com 2021/7/14 17:53
 */
@Component
@Slf4j
public class TusCorsProcessor extends DefaultCorsProcessor {

    @Autowired
    private TusProperties tusProperties;
    @Autowired
    private TusMetadataService tusMateDataService;
    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public boolean processRequest(CorsConfiguration config, HttpServletRequest request,
                                  @NonNull HttpServletResponse response) throws IOException {
        // 添加tus头
        String contextPath = request.getServletPath();
        if (!StrUtil.equals(contextPath, "/") && StrUtil.endWith(contextPath, "/")) {
            contextPath = StrUtil.removeSuffix(contextPath, "/");
        }
        if (antPathMatcher.match(tusProperties.getTusPrefix(), contextPath)) {
            this.applyTusHeader(response);
        }
        if (antPathMatcher.match(tusProperties.tusGuidAntPattern(), contextPath)) {
            UploadRecord uploadRecord = tusMateDataService.selectByGuid(StrUtil.splitToArray(contextPath, "/")[2]);
            this.applyTusHeader(response);
            response.setHeader(TusConsts.UPLOAD_OFFSET_HEADER, Objects.isNull(uploadRecord) ? "0" :
                    Long.toString(uploadRecord.getOffset()));
        }

        return super.processRequest(config, request, response);
    }

    private void applyTusHeader(HttpServletResponse response) {
        response.setHeader(TusConsts.ACCESS_CONTROL_EXPOSE_HEADER, TusConsts.ACCESS_CONTROL_EXPOSE_OPTIONS_VALUE);
        response.setHeader(TusConsts.TUS_RESUMABLE_HEADER, TusConsts.TUS_RESUMABLE_VALUE);
        response.setHeader(TusConsts.TUS_VERSION_HEADER, TusConsts.TUS_VERSION_VALUE);
        response.setHeader(TusConsts.TUS_MAX_SIZE_HEADER, tusProperties.getTusMaxSize().toString());
        response.setHeader(TusConsts.TUS_EXTENTION_HEADER, TusConsts.TUS_EXTENTION_VALUE);
        response.setHeader(TusConsts.TUS_CHECKSUM_ALGORITHM_HEADER, TusConsts.TUS_CHECKSUM_ALGORITHM_VALUE);
        response.setStatus(204);
    }
}
