package cn.lm.tus.cors;

import cn.lm.tus.consts.TusProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @author cuijiaxu@zhichubao.com 2021/7/14 17:33
 */
@Configuration
public class CorsAutoConfiguration {

    @Bean
    public CorsFilter tusCorsFilter(TusCorsProcessor tusCorsProcessor, TusProperties tusProperties) {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        config.setMaxAge(-1L);
        // CORS 配置对tus接口都有效
        source.registerCorsConfiguration(tusProperties.tusPattern(), config);
        source.registerCorsConfiguration(tusProperties.getTusPrefix(), config);
        CorsFilter filter = new CorsFilter(source);
        filter.setCorsProcessor(tusCorsProcessor);
        return filter;
    }
}
