package cn.lm.tus;

import cn.lm.tus.consts.TusProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(TusProperties.class)
@ComponentScan
public class TusAutoConfiguration {

    public static void main(String[] args) {
        SpringApplication.run(TusAutoConfiguration.class, args);
    }

}
