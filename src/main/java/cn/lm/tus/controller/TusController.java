package cn.lm.tus.controller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Locale;
import java.util.Objects;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.lm.tus.consts.TusProperties;
import cn.lm.tus.consts.TusConsts;
import cn.lm.tus.entity.UploadRecord;
import cn.lm.tus.helper.MinioHelper;
import cn.lm.tus.service.TusMetadataService;
import io.minio.messages.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author cuijiaxu@zhichubao.com 2021/7/8 14:57
 */
@RestController
@RequestMapping("#{@tusProperties.getTusPrefix()}")
public class TusController {

    public static final Logger log = LoggerFactory.getLogger(TusController.class);

    @Autowired
    private TusMetadataService tusMateDataService;
    @Autowired
    private MinioHelper minioHelper;
    @Autowired
    private TusProperties tusProperties;

    /**
     * 创建新的文件
     */
    @RequestMapping(method = RequestMethod.POST)
    public void postRequest(@RequestHeader(TusConsts.UPLOAD_LENGTH_HEADER) Integer uploadLength,
                            @RequestHeader(TusConsts.UPLOAD_METADATA_HEADER) String metadata,
                            UriComponentsBuilder uriComponentsBuilder,
                            HttpServletResponse response) {
        // 文件大小不存在
        if (uploadLength <= 0) {
            response.setStatus(412);
            return;
        }
        // 文件超过最大文件长度
        if (uploadLength > tusProperties.getTusMaxSize()) {
            response.setStatus(413);
            return;
        }

        UploadRecord uploadRecord = tusMateDataService.create(uploadLength, metadata);

        String location =
                uriComponentsBuilder.path(tusProperties.getTusPrefix() + "/" + uploadRecord.getGuid()).build().toString();

        response.setHeader(TusConsts.ACCESS_CONTROL_EXPOSE_HEADER, TusConsts.ACCESS_CONTROL_EXPOSE_POST_VALUE);
        response.setHeader(TusConsts.LOCATION_HEADER, location);
        response.setHeader(TusConsts.TUS_RESUMABLE_HEADER, TusConsts.TUS_RESUMABLE_VALUE);
        response.setHeader(TusConsts.UPLOAD_EXPIRES_HEADER, uploadRecord.getEndTime().format(DateTimeFormatter.ofPattern(TusConsts.RFC_7231).withLocale(Locale.ENGLISH).withZone(ZoneId.of("GMT+8"))));
        response.setStatus(201);
    }

    /**
     * 查询文件上传元数据信息
     */
    @RequestMapping(method = RequestMethod.HEAD, value = "/{guid}")
    public void headRequest(@PathVariable String guid, HttpServletResponse response){
        // 防止缓存头文件
        response.setHeader(TusConsts.CACHE_CONTROL_HEADER, TusConsts.CACHE_CONTROL_VALUE);

        UploadRecord uploadRecord = tusMateDataService.selectByGuid(guid);
        if (Objects.isNull(uploadRecord)) {
            // 不存在则返回404
            response.setStatus(404);
            return;
        }

        // 存在返回200，进度信息
        response.setHeader(TusConsts.ACCESS_CONTROL_EXPOSE_HEADER, TusConsts.ACCESS_CONTROL_EXPOSE_HEAD_VALUE);
        response.setHeader(TusConsts.UPLOAD_OFFSET_HEADER, Long.toString(uploadRecord.getOffset()));
        response.setHeader(TusConsts.UPLOAD_LENGTH_HEADER, Long.toString(uploadRecord.getUploadLength()));
        response.setHeader(TusConsts.TUS_RESUMABLE_HEADER, TusConsts.TUS_RESUMABLE_VALUE);
        response.setHeader(TusConsts.CACHE_CONTROL_HEADER, TusConsts.CACHE_CONTROL_VALUE);
        response.setStatus(200);
    }

    /**
     * 上传文件
     */
    @RequestMapping(method = RequestMethod.PATCH, value = "/{guid}")
    public void patchRequest(@RequestHeader(TusConsts.UPLOAD_OFFSET_HEADER) Long uploadOffset,
                             @RequestHeader(TusConsts.UPLOAD_CHECKSUM_HEADER) String uploadChecksum,
                             @RequestHeader("Content-Length") Long contentLength,
                             @RequestHeader("Content-Type") String contentType,
                             @PathVariable String guid,
                             InputStream inputStream,
                             HttpServletResponse response) {
        // 缺少当前上传长度
        if (uploadOffset == null || uploadOffset < 0) {
            response.setStatus(409);
            return;
        }
        // 缺少文件内容长度
        if (contentLength == null || contentLength < 0) {
            response.setStatus(411);
            return;
        }
        // 非流式传输
        if (!Objects.equals(contentType, "application/offset+octet-stream")) {
            response.setStatus(415);
            return;
        }

        // 文件块签名算法不正确
        Map<String, String> checksumMap = tusMateDataService.decodeMetadataHeader(uploadChecksum);
        if (checksumMap.keySet().stream().noneMatch(keys -> StrUtil.contains(TusConsts.TUS_CHECKSUM_ALGORITHM_VALUE,
                keys))) {
            response.setStatus(400);
        }
        // 文件块签名不一致
        byte[] bytes = IoUtil.readBytes(inputStream);
        if (Objects.nonNull(checksumMap.get("crc32")) &&
                !Objects.equals(Long.valueOf(checksumMap.get("crc32")),
                        IoUtil.checksumCRC32(new ByteArrayInputStream(bytes)))) {
            response.setStatus(460);
        }
        if (Objects.nonNull(checksumMap.get("sha1")) &&
                !Objects.equals(checksumMap.get("sha1"), SecureUtil.sha1(new ByteArrayInputStream(bytes)))) {
            response.setStatus(409);
            return;
        }

        UploadRecord uploadRecord = tusMateDataService.selectByGuid(guid);
        // 不存在则返回404
        if (Objects.isNull(uploadRecord)) {
            response.setStatus(404);
            return;
        }
        // 已上传长度和记录长度不一致
        if (!Objects.equals(uploadOffset, uploadRecord.getOffset())) {
            response.setStatus(409);
            return;
        }
        // 文件已上传长度大于文件长度
        if (uploadRecord.getUploadLength() < uploadRecord.getOffset()) {
            response.setStatus(409);
            return;
        }
        // 文件上传记录已过期
        if (LocalDateTime.now().isAfter(uploadRecord.getEndTime())) {
            tusMateDataService.remove(uploadRecord);
            response.setStatus(410);
            return;
        }

        UploadRecord upload = tusMateDataService.upload(uploadRecord, uploadOffset, new ByteArrayInputStream(bytes));

        response.setHeader(TusConsts.ACCESS_CONTROL_EXPOSE_HEADER, TusConsts.ACCESS_CONTROL_EXPOSE_PATCH_VALUE);
        response.setHeader(TusConsts.TUS_RESUMABLE_HEADER, TusConsts.TUS_RESUMABLE_VALUE);
        response.setHeader(TusConsts.UPLOAD_OFFSET_HEADER, Long.toString(upload.getOffset()));
        response.setHeader(TusConsts.UPLOAD_EXPIRES_HEADER, uploadRecord.getEndTime().format(DateTimeFormatter.ofPattern(TusConsts.RFC_7231).withLocale(Locale.ENGLISH).withZone(ZoneId.of("GMT+8"))));
        response.setStatus(204);
    }

    /**
     * 文件下载，根据 Guid
     */
    @RequestMapping(path = "/{guid}", method = RequestMethod.GET)
    public void getFileByGuid(@PathVariable String guid, HttpServletResponse response) throws Exception {
        UploadRecord uploadRecord = tusMateDataService.getCompletedFileByGuid(guid);
        response.sendRedirect(minioHelper.getSignUrl(uploadRecord.getBucket(), uploadRecord.getFileName()));
    }

    /**
     * 文件下载，根据 文件名
     */
    @RequestMapping(path = "/file/{bucket}/{fileName}", method = RequestMethod.GET)
    public void getFileByFileName(@PathVariable String bucket, @PathVariable String fileName, HttpServletResponse response) throws Exception {
        response.sendRedirect(minioHelper.getSignUrl(bucket, fileName));
    }

    /**
     * 获取桶内文件列表
     */
    @RequestMapping(path = "/file/{bucket}", method = RequestMethod.GET)
    public ResponseEntity<List<Item>> getFileByFileName(@PathVariable String bucket) {
        return new ResponseEntity<>(minioHelper.getFileListByBucket(bucket), HttpStatus.OK);
    }


}
