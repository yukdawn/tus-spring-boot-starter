package cn.lm.tus.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import cn.lm.tus.consts.TusProperties;
import cn.lm.tus.consts.TusConsts;
import cn.lm.tus.entity.UploadRecord;
import cn.lm.tus.helper.MinioHelper;
import cn.lm.tus.service.TusMetadataService;
import io.minio.ComposeSource;
import io.minio.GetObjectResponse;
import io.minio.ObjectWriteResponse;
import io.minio.StatObjectResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

/**
 * @author cuijiaxu@zhichubao.com 2021/7/8 15:46
 */
@Repository
public class TusMetadataServiceImpl implements TusMetadataService {

    @Autowired
    private MinioHelper minioHelper;
    @Autowired
    private TusProperties tusProperties;

    @Override
    public UploadRecord upload(UploadRecord uploadRecord, Long uploadOffset, InputStream in) {
        // 检查文件是否完成，若相等则完成文件上传
        UploadRecord completed = completeFileUpload(uploadRecord);
        if (completed.isCompleted()) {
            return completed;
        }

        // 上传文件分片
        long newOffset = this.uploadPart(uploadRecord, in);
        if (newOffset > uploadRecord.getUploadLength()) {
            throw new RuntimeException("文件上传后超长");
        }
        uploadRecord.setOffset(newOffset);
        this.save(uploadRecord);
        return uploadRecord;
    }

    @Override
    public UploadRecord getCompletedFileByGuid(String guid) {
        UploadRecord uploadRecord = this.selectByGuid(guid);
        if(uploadRecord == null){
            throw new RuntimeException("文件未找到");
        }
        if (!uploadRecord.isCompleted() && Objects.equals(uploadRecord.getUploadLength(), uploadRecord.getOffset())) {
            uploadRecord.setCompleted(true);
            this.save(uploadRecord);
        }
        if (!uploadRecord.isCompleted()) {
            throw new RuntimeException("文件上传未完成");
        }
        return uploadRecord;
    }

    @Override
    public void remove(UploadRecord uploadRecord) {
        // 删除part文件
        minioHelper.removeObject(TusConsts.TUS_BUCKET, uploadRecord.partFile());
        // 删除main文件
        minioHelper.removeObject(TusConsts.TUS_BUCKET, uploadRecord.mainFile());
        // 删除guid文件
        minioHelper.removeObject(TusConsts.TUS_BUCKET, uploadRecord.getGuid());
    }

    @Override
    public UploadRecord completeFileUpload(UploadRecord uploadRecord) {
        // 文件长度相等且未完成
        if (Objects.equals(uploadRecord.getUploadLength(), uploadRecord.getOffset()) && !uploadRecord.isCompleted()) {
            uploadRecord.setCompleted(true);
            this.save(uploadRecord);
            return uploadRecord;
        }
        return uploadRecord;
    }

    @Override
    public UploadRecord selectByGuid(String guid) {
        GetObjectResponse object = minioHelper.getObject(TusConsts.TUS_BUCKET, guid);
        if (Objects.isNull(object)){
            return null;
        }
        String uploadFileStr = IoUtil.read(object, Charset.defaultCharset());
        return JSONUtil.toBean(uploadFileStr, UploadRecord.class);
    }

    @Override
    public UploadRecord create(Integer uploadLength, String metadataHeader) {


        Map<String, String> matedataHeader = this.decodeMetadataHeader(metadataHeader);
        UploadRecord uploadRecord = new UploadRecord(matedataHeader, uploadLength, tusProperties.getTusUploadTimeout());
        // 保存元数据文件
        updateUploadFile(uploadRecord);
        return uploadRecord;
    }

    @Override
    public void save(UploadRecord uploadRecord) {
        UploadRecord source = this.selectByGuid(uploadRecord.getGuid());
        Assert.notNull(source, "文件guid错误");
        // 检查目标文件是否存在
        StatObjectResponse targetExist = minioHelper.statObject(uploadRecord.getBucket(), uploadRecord.uniqueKey());
        if (Objects.equals(source.getUploadLength(), uploadRecord.getOffset()) || Objects.nonNull(targetExist)) {
            // 检查源记录中文件大小是否和当前文件大小相当，或目标文件已经存在。将文件移动到对应桶并更新元数据文件为已完成
            // 设置为已完成
            source.setCompleted(true);
            // 文件复制到目标桶
            ObjectWriteResponse result = minioHelper.copyObjectIfNotExist(TusConsts.TUS_BUCKET,
                    uploadRecord.mainFile(), uploadRecord.getBucket(), uploadRecord.uniqueKey());
            if (!Objects.isNull(result)) {
                // 删除main文件
                minioHelper.removeObject(TusConsts.TUS_BUCKET, uploadRecord.mainFile());
            }
        }
        // 将新的进度文件覆盖
        source.setOffset(uploadRecord.getOffset());
        // 更新元数据文件
        updateUploadFile(uploadRecord);
    }

    @Override
    public Map<String, String> decodeMetadataHeader(String metadataHeader) {
        Map<String, String> map = new HashMap<>();
        map.put("bucket", TusConsts.DEFAULT_BUCKET);
        String[] entrys = metadataHeader.split(",");
        for (String entry : entrys) {
            String[] kv = entry.split(" ");
            map.put(kv[0], new String(Base64.getDecoder().decode(kv[1])));
        }
        return map;
    }

    private void updateUploadFile(UploadRecord uploadRecord){
        ByteArrayInputStream bais = new ByteArrayInputStream(StrUtil.bytes(JSONUtil.toJsonStr(uploadRecord)));
        minioHelper.putObject(TusConsts.TUS_BUCKET, uploadRecord.getGuid(), bais);
    }

    /**
     * 大概是幂等的
     *
     * @param uploadRecord 文件上传记录
     * @param in           文件流
     * @return 文件当前长度
     */
    private long uploadPart(UploadRecord uploadRecord, InputStream in) {
        Assert.notNull(uploadRecord, "资源未创建");
        if (Objects.equals(uploadRecord.getOffset(), 0L)){
            // 第一次上传则创建主分片文件
            minioHelper.putObject(TusConsts.TUS_BUCKET, uploadRecord.mainFile(), in);
            // 获取主分片文件的文件元数据
            StatObjectResponse statObjectResponse = minioHelper.statObject(TusConsts.TUS_BUCKET,
                    uploadRecord.mainFile());
            // 返回文件字节
            return statObjectResponse.size();
        }
        // 后续上传则创建分片文件，上传完毕后合并
        minioHelper.putObject(TusConsts.TUS_BUCKET, uploadRecord.partFile(), in);
        // 合并文件
        ComposeSource main = ComposeSource.builder()
                .bucket(TusConsts.TUS_BUCKET)
                .object(uploadRecord.mainFile()).build();
        ComposeSource part = ComposeSource.builder()
                .bucket(TusConsts.TUS_BUCKET)
                .object(uploadRecord.partFile()).build();
        // 将文件合并到主临时文件
        minioHelper.composeObject(TusConsts.TUS_BUCKET, uploadRecord.mainTempFile(), CollUtil.toList(main, part));
        // 将源文件移除
        minioHelper.removeObject(TusConsts.TUS_BUCKET, uploadRecord.mainFile());
        minioHelper.removeObject(TusConsts.TUS_BUCKET, uploadRecord.partFile());
        // 重命名主临时文件
        minioHelper.renameObject(TusConsts.TUS_BUCKET, uploadRecord.mainTempFile(), uploadRecord.mainFile());
        // 获取合并后的文件元数据
        StatObjectResponse statObjectResponse = minioHelper.statObject(TusConsts.TUS_BUCKET, uploadRecord.mainFile());
        statObjectResponse.size();
        // 删除part文件
        minioHelper.removeObject(TusConsts.TUS_BUCKET, uploadRecord.partFile());
        return statObjectResponse.size();
    }
}
