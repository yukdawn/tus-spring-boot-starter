package cn.lm.tus.service;

import java.io.InputStream;
import java.util.Map;

import cn.lm.tus.entity.UploadRecord;

/**
 * @author cuijiaxu@zhichubao.com 2021/7/8 15:46
 */
public interface TusMetadataService {

    /**
     * <p color="green">此方法幂等</p>
     * <br />
     * <p>根据guid查询文件上传记录</p>
     * <p>若文件不存在，则返回空</p>
     * @param guid 文件上传记录全局唯一ID
     * @return UploadRecord
     */
    UploadRecord selectByGuid(String guid);

    /**
     * <p color="green">此方法幂等</p>
     * <br />
     * <p>创建文件上传记录并返回</p>
     * @param uploadLength 即将上传文件的总长度
     * @param metadataHeader tus元数据http header
     * @return UploadRecord
     */
    UploadRecord create(Integer uploadLength, String metadataHeader);

    /**
     * 解码元数据http header头并以map形式返回
     * @param metadataHeader tus header头
     * @return 转换为map形式的tus header
     */
    Map<String, String> decodeMetadataHeader(String metadataHeader);

    /**
     * <p color="green">此方法幂等</p>
     * <br />
     * <p>保存文件元数据及文分片文件。</p>
     * <p>若文件未上传完成，则会将文件合并到主分片文件并更新文件上传记录</p>
     * <p>若文件上传完成，则会将文件移动到目标桶并移除文件上传记录</p>
     *
     * @param uploadRecord 文件上传记录
     */
    void save(UploadRecord uploadRecord);

    /**
     * 完成文件上传
     * 方法内部会检查当前文件大小与上传记录中大小是否一直
     * 一致则完成文件上传
     * 不一致则直接返回原对象
     * @param uploadRecord 文件上传记录
     * @return 文件上传记录
     */
    UploadRecord completeFileUpload(UploadRecord uploadRecord);

    /**
     * <p color="green">此方法幂等</p>
     * <br />
     * <p>将将文件保存至minio，并更新文件上传记录</p>
     *
     * @param uploadRecord 文件上传记录
     * @param uploadOffset 当前偏移量
     * @param in           本次文件流
     */
    UploadRecord upload(UploadRecord uploadRecord, Long uploadOffset, InputStream in);

    /**
     * 根据guid获取已上传完成文件
     *
     * @param guid 文件上传记录全局唯一ID
     * @return 文件上传记录
     */
    UploadRecord getCompletedFileByGuid(String guid);

    void remove(UploadRecord uploadRecord);
}
