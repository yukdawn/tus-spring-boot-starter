package cn.lm.tus.helper;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;

import cn.hutool.core.io.IoUtil;
import cn.lm.tus.consts.TusProperties;
import cn.lm.tus.consts.TusConsts;
import cn.lm.tus.exception.FileServerException;
import io.minio.*;
import io.minio.http.Method;
import io.minio.messages.Item;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author cuijiaxu@zhichubao.com 2021/7/9 10:44
 */
@Component
@Slf4j
public class MinioHelper implements InitializingBean {

    private MinioClient minioClient;
    @Autowired
    private TusProperties tusProperties;

    @Override
    public void afterPropertiesSet() {
        minioClient = MinioClient.builder()
                .endpoint(tusProperties.getMinioHost())
                .credentials(tusProperties.getMinioUsername(), tusProperties.getMinioPassword())
                .build();
        makeBucketIfNotExist(TusConsts.TUS_BUCKET);
    }

    public void removeObject(String bucket, String object){
        try {
            // 检测当前文件是否存在，不存在则直接返回
            if (Objects.isNull(this.statObject(bucket, object))){
                return;
            }
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucket)
                    .object(object).build());
        } catch (Exception e) {
            throw new FileServerException(e);
        }
    }

    public void putObject(String bucket, String object, InputStream inputStream) {
        try {
            byte[] bytes = IoUtil.readBytes(inputStream);
            ByteArrayInputStream bs = new ByteArrayInputStream(bytes);
            minioClient.putObject(PutObjectArgs.builder()
                    .bucket(bucket)
                    .object(object)
                    .stream(bs, bytes.length, -1)
                    .contentType("application/octet-stream").build());
        } catch (Exception e) {
            throw new FileServerException(e);
        }
    }

    public StatObjectResponse statObject(String bucket, String object){
        try {
            return minioClient.statObject(StatObjectArgs.builder().bucket(bucket)
                            .object(object).build());
        } catch (Exception e) {
            log.debug("文件元数据获取异常", e);
            return null;
        }
    }

    public void composeObject(String bucket, String object, List<ComposeSource> composeSourceList) {
        try {
            // 检测目标文件是否已经存在，存在则直接返回
            if (Objects.nonNull(this.statObject(bucket, object))) {
                return;
            }
            minioClient.composeObject(ComposeObjectArgs.builder()
                    .bucket(bucket)
                    .object(object).sources(composeSourceList).build());
        } catch (Exception e) {
            throw new FileServerException(e);
        }

    }

    public String getSignUrl(String bucket, String object){
        Map<String, String> reqParams = new HashMap<>(16);
        reqParams.put("response-content-type", "application/octet-stream");
        reqParams.put("response-content-disposition", "attachment;filename=" + Base64.getEncoder().encodeToString(object.getBytes(StandardCharsets.UTF_8)));
        reqParams.put("response-cache-control", "must-revalidate, post-check=0, pre-check=0");
        reqParams.put("response-expires", String.valueOf(System.currentTimeMillis() + 1000));
        try {
            return minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                    .method(Method.GET)
                    .bucket(bucket)
                    .object(object)
                    .expiry(300, TimeUnit.MINUTES)
                    .extraQueryParams(reqParams)
                    .build());
        } catch (Exception e) {
            throw new FileServerException(e);
        }
    }

    public GetObjectResponse getObject(String bucket, String object){
        GetObjectArgs tus = GetObjectArgs.builder().bucket(bucket).object(object).build();
        try {
            return minioClient.getObject(tus);
        } catch (Exception e) {
            log.warn("File Get Exception", e);
            return null;
        }
    }

    /**
     * 如果目标桶不存在该文件则进行复制
     *
     * @param srcBucket    源桶
     * @param srcObject    源文件
     * @param targetBucket 目标桶
     * @param targetObject 目标文件
     */
    public ObjectWriteResponse copyObjectIfNotExist(String srcBucket, String srcObject, String targetBucket,
                                                    String targetObject) {
        try {
            // 检查源文件是否存在
            StatObjectResponse sourceExist = this.statObject(srcBucket, srcObject);
            if (Objects.isNull(sourceExist)) {
                // 不存在则返回null
                return null;
            }
            StatObjectResponse targetExist = this.statObject(targetBucket, targetObject);
            if (Objects.nonNull(targetExist)) {
                // 不为空则返回已存在文件描述
                return new ObjectWriteResponse(targetExist.headers(), targetExist.bucket(), targetExist.region(),
                        targetExist.object(), targetExist.etag(), targetExist.versionId());
            }
            // 如果桶不存在则创建目标桶
            this.makeBucketIfNotExist(targetBucket);
            return minioClient.copyObject(
                    CopyObjectArgs.builder()
                            .bucket(targetBucket)
                            .object(targetObject)
                            .source(CopySource.builder().bucket(srcBucket).object(srcObject).build())
                            .build());
        } catch (Exception e) {
            throw new FileServerException(e);
        }
    }

    public void renameObject(String bucket, String oldObject, String newObject){
        this.copyObjectIfNotExist(bucket, oldObject, bucket, newObject);
        this.removeObject(bucket, oldObject);
    }

    public void makeBucketIfNotExist(String bucket) {
        try {
            boolean exists = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucket).build());
            if (!exists){
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucket).build());
            }
        } catch (Exception e) {
            throw new FileServerException(e);
        }
    }

    public List<Item> getFileListByBucket(String bucket){
        Iterator<Result<Item>> iterator =
                minioClient.listObjects(ListObjectsArgs.builder().bucket(bucket).build()).iterator();
        List<Item> itemList = new ArrayList<>();
        while (iterator.hasNext()){
            try {
                itemList.add(iterator.next().get());
            } catch (Exception e) {
                log.warn("Get file list error.", e);
            }
        }
        return itemList;
    }
}
