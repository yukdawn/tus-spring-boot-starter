# tus-spring-boot-starter

#### 介绍
实现tus协议的starter，存储中间件使用minio
master分支实现了creation,expiration,extension
no-checksum分支实现了creation,expiration，可配合uppy使用


#### 安装教程

```shell
mvn clean install
```

#### 使用说明
##### 后端
- 引入
```xml
<dependency>
    <groupId>cn.lm</groupId>
    <artifactId>tus</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```
##### 添加配置
```yaml
tus:
  tusPrefix: /tus
  minioHost: http://localhost:9000
  minioUsername: minioadmin
  minioPassword: minioadmin
```

前端使用uppy进行上传
```js
const uppy = new Uppy({meta: {bucket: 'itm'}}).use(Tus, {
        endpoint: 'http://127.0.0.1:8080/tus', // use your tus endpoint here
        resume: true,
        retryDelays: null,
        chunkSize: 5*1024*1024,
      });
```